import json
import socket
from functools import wraps
import time


class WebSockets:

    def __init__(self, socket_type, port, host):
        self.socket_type = socket_type
        self.port = port
        self.host = host

    def set_configuration(self, socket):
        return self.socket_type.set_configuration(self.host, self.port, socket)

    @staticmethod
    def create_socket():
        return socket.socket()


class ClientSocket:

    @staticmethod
    def set_configuration(host, port, socket_instance):
        endpoint = (host, port)
        socket_instance.connect(endpoint)

    @staticmethod
    def send_message(message, socket):
        socket.send(message)


class ServerSocket:

    @staticmethod
    def set_configuration(host, port, socket_instance):
        endpoint = (host, port)
        socket_instance.bind(endpoint)
        socket_instance.listen(5)

    @staticmethod
    def send_message(message, connection):
        connection.send(message)


class Implementation:
    
    socket_manager = WebSockets(ServerSocket(), 5000, socket.gethostname())
    socket_instance = socket_manager.create_socket()
    socket_manager.set_configuration(socket_instance)
    print("Raising server socket...")
    @classmethod
    def raise_server_socket(cls):

        try:
            while True:
                conexion, address = cls.socket_instance.accept()
                while True:
                    conexion.send(json.dumps({"incident_id":"03dba0df-8b54-4dd9-9c76-4f421df21181", "percent": 35}).encode())
                    time.sleep(10)
                    conexion.send(json.dumps({"incident_id":"03dba0df-8b54-4dd9-9c76-4f421df21181", "percent": 70}).encode())
                    time.sleep(10)
                    conexion.send(json.dumps({"incident_id":"03dba0df-8b54-4dd9-9c76-4f421df21181", "percent": 90}).encode())
                    time.sleep(10)
                    conexion.send(json.dumps({"incident_id":"03dba0df-8b54-4dd9-9c76-4f421df21181", "percent": 10}).encode())
                    time.sleep(10)
        except Exception:
            cls.raise_server_socket()
        

